# installation

1. Install and setup golang: https://go.dev/doc/install
1. `git clone https://gitlab.com/kostrahb/russia/`
1. `cd russia`
1. `go get`

# Usage

Set concurency by page in pages.yaml, then run
`go run main.go pages.yaml`

# Update

1. `git stash`
1. `git pull`
1. `git stash pop`
1. If needed, correct conflicts (probably in pages.yaml)

# Useful

`cat tmp | cut -f 1 | awk '{ printf "  %s: 50\n", $1 }'`