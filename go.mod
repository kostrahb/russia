module gitlab.com/kostrahb/russia

go 1.17

require (
	github.com/Konstantin8105/DDoS v0.0.0-20190217121645-5cb204463537
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/Konstantin8105/FreePort v0.0.0-20170920080630-ee2c5b53b80f // indirect
	github.com/corpix/uarand v0.1.1 // indirect
)
